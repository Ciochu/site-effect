$(document).ready(() => {
    var map = L.map('mapid').setView([53.982679,20.3832657], 7);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    //Bialystok
    L.marker([53.1387838, 23.1841466]).addTo(map);
    //Gdynia
    L.marker([54.4706382, 18.5174727]).addTo(map);
});